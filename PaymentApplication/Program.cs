using System;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace PaymentApplication
{
    using PaymentApplication.Entities;
    using PaymentApplication.Services;

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}


/*
 * В спецификации нет примера использования метода создания платежной сессии,
 * принимающего сумму и назначение платежа. Спецификация должна отражать пример реального использования. 
 * Не соблюдаются принципы JSON.API. https://jsonapi.org/format/
 * Ответ от сервера в формате текста. Клиентскому приложению будет сложно реагировать на подобные ошибки.
 * Все ошибки возвращают код 404. В списке кодов состояний HTTP имеется большое количество кодов ошибок клиента. 
 * https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%BA%D0%BE%D0%B4%D0%BE%D0%B2_%D1%81%D0%BE%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D1%8F_HTTP
 * Комментарии на разных языках.
*/
*/