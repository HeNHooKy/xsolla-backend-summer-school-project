FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["PaymentApplication/PaymentApplication.csproj", "PaymentApplication/"]
RUN dotnet restore "PaymentApplication/PaymentApplication.csproj"
COPY . .
WORKDIR "/src/PaymentApplication"
RUN dotnet build "PaymentApplication.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PaymentApplication.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PaymentApplication.dll"]
