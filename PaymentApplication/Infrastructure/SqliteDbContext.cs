﻿namespace PaymentApplication.Infrastructure
{
    using System.Reflection;

    using Microsoft.EntityFrameworkCore;

    using PaymentApplication.Entities;

    public class SqliteDbContext : DbContext
    {
        public DbSet<Session> Session { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=..\\Database\\database.db", options =>
            {
                options.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName);
            });
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Map table names
            modelBuilder.Entity<Session>().ToTable("Session", "test");
            modelBuilder.Entity<Session>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Expires);
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}