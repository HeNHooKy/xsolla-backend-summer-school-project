﻿namespace PaymentApplication.Services
{
    using System;

    using PaymentApplication.Entities;

    /// <summary>
    /// Сервис проверки валидности платежной сессии.
    /// </summary>
    public interface ISessionService
    {
        /// <summary>
        /// Добавляет новую платежную сессию в БД.
        /// </summary>
        /// <param name="session">Новая сессия.</param>
        void Add(Session session);

        /// <summary>
        /// Проверяет валидность сессии.
        /// </summary>
        /// <param name="sessionId">Id сессии.</param>
        /// <returns><c>true</c>, если сессия валидна, иначе - <c>false</c>.</returns>
        bool IsValid(Guid sessionId);
    }
}