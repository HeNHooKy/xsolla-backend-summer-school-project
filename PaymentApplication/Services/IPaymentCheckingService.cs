﻿namespace PaymentApplication.Services
{
    /// <summary>
    /// Сервис проверки карты.
    /// </summary>
    public interface IPaymentCheckingService
    {
        /// <summary>
        /// Проверяет валидность карты.
        /// </summary>
        /// <param name="number">Номер карты.</param>
        /// <param name="expires">Срок действия.</param>
        /// <param name="cvc">CVC-код.</param>
        /// <returns><c>true</c>, если карта валидна. В противном случае - <c>false</c>.</returns>
        bool IsValidCard(string number, string expires, string cvc);
    }
}