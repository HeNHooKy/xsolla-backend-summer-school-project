﻿namespace PaymentApplication.Services
{
    using System;

    using PaymentApplication.Entities;

    /// <inheritdoc />
    public class PaymentCheckingService : IPaymentCheckingService
    {
        /// <inheritdoc />
        public bool IsValidCard(string number, string expires, string cvc)
        {
            var creditCard = new CreditCard(number, expires, cvc);

            return IsValidByLuhn(creditCard) && DateTime.UtcNow <= creditCard.Expires;
        }

        private bool IsValidByLuhn(CreditCard creditCard)
        {
            var number = creditCard.Number;
            int sum = 0;
            for (int i = number.Length - 1; i >= 0; i--)
            {
                if (i % 2 == 0)
                {
                    number[i] *= 2;
                }

                if (number[i] > 9)
                {
                    number[i] -= 9;
                }

                sum += number[i];
            }

            return sum % 10 == 0;
        }
    }
}