﻿namespace PaymentApplication.Models
{
    public class PaymentRequestModel
    {
        public string CardNumber { get; set; }

        public string CardCVC { get; set; }
        
        public string CardExpirationDate { get; set; }

        public string SessionId { get; set; }
    }
}