﻿namespace PaymentApplication.Entities
{
    using System;
    using System.Linq;

    public class CreditCard
    {
        public int[] Number { get; }

        public DateTime Expires { get; }

        public string CVC { get; }

        public CreditCard(string number, string expires, string cvc)
        {
            Number = ConvertToCorrectCardNumber(number);
            Expires = ConvertToCorrectExpirationDate(expires);

            //Почему бы не использовать конструкцию со схожим Number и Expires стилем?
            if (!CheckCVC(cvc))
            {
                throw new ArgumentException("Некорректный CVC-код.");
            }

            CVC = cvc;
        }

        private int[] ConvertToCorrectCardNumber(string number)
        {
            var numArray = number
                .Replace(" ", "")//Trim()
                .Select(x => int.TryParse(x.ToString(), out int res) ? res : -1)
                .Where(x => x >= 0)
                .ToArray();

            if (numArray.Length != 16)
            {
                throw new ArgumentException("Некорректный номер карты.");
            }

            return numArray;
        }

        private DateTime ConvertToCorrectExpirationDate(string expires)
        {
            var monthAndYear = expires
                .Split("/")
                .Select(x => int.TryParse(x, out int res) ? res : -1)
                .Where(x => x > 0 && x < 100) // Берем только те, что привелись к int и содержат не больше двух цифр в месяце и годе (2000 год тоже исключим).
                .ToList();

            if (monthAndYear.Count != 2)
            {
                throw new ArgumentException("Некорректный срок действия карты.");
            }

            int month = monthAndYear.First();
            int year = monthAndYear.Last();

            if (month > 12)
            {
                throw new ArgumentException("Указан некорректный месяц в сроке действия карты.");
            }

            // Дата истечения срока действия карты равна первому числу после месяца, указанного на карте.
            return new DateTime(2000 + year, month + 1, 1);
        }

        private bool CheckCVC(string cvc)
        {
            if (cvc.Length != 3)
            {
                return false;
            }

            return int.TryParse(cvc, out int res) && res > 0;
        }
    }
}